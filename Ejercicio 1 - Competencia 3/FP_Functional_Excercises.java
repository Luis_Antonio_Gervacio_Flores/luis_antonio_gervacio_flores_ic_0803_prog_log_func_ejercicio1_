import java.util.List;
public class FP_Functional_Excercises {
    public static void main(String[] args) {
        List<Integer> numbers = List.of (12, 9, 13, 4, 6, 2, 4, 12, 15);

        List<String> courses = List.of("Spring","Spring Boot","API", "Microservices", "AWS", "PCF","Azure", "Docker","Kubernetes");

        List<String> cursos = List.of("Hola","Agua","Mapa", "Lupe", "Rana", "Toro");

        System.out.println("Ejercicio 1");
        numbers.stream()
        .filter(FP_Functional_Excercises::numeroImpar) 
        .forEach(numeros -> System.out.println("" + numeros));
       
        System.out.println("\n");

        System.out.println("Ejercicio 2");
        courses.stream()
        .forEach(le -> System.out.println(le + ", "));
        System.out.println("\n");

        System.out.println("Ejercicio 3");
        courses.stream()
        .filter(palabra -> palabra.contains("Spring"))
        .forEach(FP_Functional_Excercises::print);
        System.out.println("\n");

        System.out.println("Ejercicio 4");
        cursos.stream()
        .forEach(Ej4 -> System.out.println(Ej4 + ""));
        System.out.println("\n");

        System.out.println("Ejercicio 5");
        numbers.stream();
        for (int i = 0; i < numbers.size(); i++){
            int cb = (int)numbers.get(i);
            if (cb % 2 != 0){
                int cubo= (int)Math.pow(cb, 3);
                System.out.println(cubo);
            }
        }
        System.out.println("\n");

        System.out.println("Ejercicio 6");
        courses.stream()
        for (int i = 0; i < frase.legth(); i++){
            char letra = frase.charAT(i);
            for (int j = 0; j < letras.length(); j++){
                if (letra == letras [j]){
                    numeroLetras++;
                    break;
                }
            }
        }
        System.out.println("\n");

   }
   private static boolean numeroImpar(int entero){
       return (entero % 2 !=0);
   }
   private static void print (String palabra){
       System.out.println(palabra + " <- Resultado ");
   }
   
}
